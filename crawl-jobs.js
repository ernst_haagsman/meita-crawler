var Crawler = require("crawler");
var url = require('url');
var fs = require('fs');

// Put the initial URL as the zeroth element of this array
var previousURLs = ['http://www.startuphire.com/search/index.php?searchId=1db7d382860e092b48908a33c79470ae&L=0'];

// The script will fill this with the '/job/' URLs
var jobListingURLs = [];

var c = new Crawler({
    debug: true,
    maxConnections : 1,
    userAgent: "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0",
    // This will be called for each crawled page
    callback : function (error, result, $) {
        // $ is Cheerio by default
        //a lean implementation of core jQuery designed specifically for the server
        $('a').each(function(index,a){
            analyzeURL(index, a, $)
        });
    }
});

function analyzeURL(index, a, $) {
    var newUrl = $(a).attr('href');
    if(typeof(newUrl) !== 'string' || newUrl.length < 2) return;

    var toQueueUrl = url.parse(newUrl, true);
    if(/*toQueueUrl.hostname !== "www.startuphire.com" &&*/
        toQueueUrl.hostname !== null) return;

    queueIfApplicable(toQueueUrl);
}

function queueIfApplicable(toQueueUrl){
    var urlString = url.format(toQueueUrl);

    // Add jobs to 'jobListingURLs' and queue further results
    if(toQueueUrl.pathname.indexOf('/job/') !== -1){
        if(jobListingURLs.indexOf(urlString) !== -1) return;
        jobListingURLs.push(urlString);
        console.log(urlString);
    } else if (toQueueUrl.pathname.indexOf('/search/') !== -1 && toQueueUrl.query &&
        toQueueUrl.query['searchId'] == '1db7d382860e092b48908a33c79470ae') {

        // For some reason on this site the pagination will result in crap like &L=40&L=60
        // The easiest way to deal with it is to recursively call ourselves with each
        // As we're keeping track of pages we have previously visited they won't be visited multiple times anyway
        if (toQueueUrl.query.L && Array.isArray(toQueueUrl.query.L)){
            toQueueUrl.query.L.forEach(function(elem){
                var newURL = toQueueUrl;
                newURL.protocol = 'http';
                newURL.hostname = 'www.startuphire.com';
                newURL.search = null;
                newURL.query.L = elem;
                queueIfApplicable(newURL);
            });
            return;
        }

        // No duplicates allowed
        if(previousURLs.indexOf(urlString) !== -1) return;
        console.log(urlString);
        previousURLs.push(urlString);
        c.queue(url.format(toQueueUrl));
    }
}

// Start the process by queueing the initial URL
c.queue(previousURLs[0]);

// As we're finished, save the whole thing in a JSON file
c.on('pool:drain', function(){
    var jsonString = JSON.stringify({
        jobs : jobListingURLs
    });

    fs.writeFile('jobs.json', jsonString);
});