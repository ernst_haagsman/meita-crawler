var fs = require('fs');
var cheerio = require('cheerio');
var path = require('path');

var NL = require('os').EOL;

/*
 *  We'll attempt to read all the HTML files and create a CSV file which should be easier for rapidminer...
 */

/**
 * Define which fields to obtain with this array of objects
 * Each objects defines the 'title' which is put at the beginning of the CSV file
 * The fetch function will receive the cheerio object of the file, and should return a string with the info
 * that should be dumped into the CSV
 */
var fields = [
    {
        name: "title",
        fetch: function($){
            return $('span[itemprop="title"]').text();
        }
    },
    {
        name: "company",
        fetch: function($){
            return $('span[itemprop="hiringOrganization"]').text();
        }
    },
    {
        name: "description",
        fetch: function($){
            return $('span[itemprop="description"]').text();
        }
    },
    {
        name: "address",
        fetch: function($){
            return $('td[itemprop="jobLocation"]').text();
        }
    }
];

var folder = 'result';
var output = 'jobs.csv';

// END OF CONFIGURATION

var header = ['filename'].concat(fields.map(function(elem){
    return elem.name;
})).join().concat(NL);

fs.writeFileSync(output, header);

var files = fs.readdirSync(folder);

files.forEach(function(file){
    var fileName = path.join(folder, file);
    fs.readFile(fileName, function(err, contents){
        var dom = cheerio.load(contents);
        var line = [file].concat(fields.map(function(elem){
            // Wrap the result in quotes, make it single-line (RapidMiner shits itself with multiline stuff), and
            // escape existing quotes with an additional quote sign.
            return '"' + elem.fetch(dom).replace(/\n/g, ' ').replace(/"/g, '""') + '"';
        })).join().concat(NL);
        fs.appendFile(output, line);
    })
});