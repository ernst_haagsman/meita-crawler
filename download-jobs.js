var Crawler = require("crawler");
var url = require('url');
var fs = require('fs');
var rimraf = require('rimraf');

// The process is separated because the first one should be a single thread, this website has a strange habit
// of storing all kinds of crap in that long 'searchId' string
var c = new Crawler({
    debug: true,
    maxConnections : 10,
    userAgent: "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0",
    // This will be called for each crawled page
    callback : function (error, result, $) {
        var body = result.body;
        var uri = url.parse(result.uri);

        fs.writeFile('./result/' + uri.pathname.substr(5) + '.html', body);
    }
});

// Delete directory if exists
rimraf.sync('./result');

// Create directory for the results
fs.mkdirSync('result');

var urlFile = fs.readFileSync('jobs.json');
var URLlist = JSON.parse(urlFile).jobs;
URLlist.forEach(function(elem){
    var newUrl = url.parse(elem);
    newUrl.protocol = 'http';
    newUrl.hostname = newUrl.hostname || 'www.startuphire.com';
    c.queue(url.format(newUrl));
});